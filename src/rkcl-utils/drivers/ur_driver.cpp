#include <rkcl/drivers/ur_driver.h>

#include <ur/driver.h>
#include <yaml-cpp/yaml.h>

#include <iostream>
#include <stdexcept>

using namespace rkcl;

bool URDriver::registered_in_factory = DriverFactory::add<URDriver>("ur");

struct URDriver::pImpl
{
    pImpl(double sample_time, const std::string& ip_address, int port)
        : sample_time(sample_time), driver(robot, ip_address, port)
    {
    }
    double sample_time;
    ur::Robot robot;
    ur::Driver driver;
};

URDriver::URDriver(const std::string& ip, int port, JointGroupPtr joint_group, ObservationPointPtr op_eef)
    : JointsDriver(joint_group), ForceSensorDriver(op_eef)
{
    impl_ = std::make_unique<URDriver::pImpl>(joint_group_->controlTimeStep(), ip, port);
}

URDriver::URDriver(const std::string& ip, int port, JointGroupPtr joint_group)
    : URDriver(ip, port, joint_group, nullptr)
{
}

URDriver::URDriver(Robot& robot, const YAML::Node& configuration)
{
    if (configuration)
    {
        std::string ip;
        int port;
        try
        {
            ip = configuration["ip"].as<std::string>();
        }
        catch (...)
        {
            throw std::runtime_error("URDriver::URDriver: You must provide a 'ip' field in the ur configuration.");
        }
        try
        {
            port = configuration["port"].as<double>();
        }
        catch (...)
        {
            throw std::runtime_error("URDriver::URDriver: You must provide a 'port' field in the ur configuration.");
        }

        std::string joint_group;
        try
        {
            joint_group = configuration["joint_group"].as<std::string>();
        }
        catch (...)
        {
            throw std::runtime_error("DualKukaLWRFRIDriver::DualKukaLWRFRIDriver: You must provide a 'joint_group' field");
        }
        joint_group_ = robot.jointGroup(joint_group);
        if (not joint_group_)
            throw std::runtime_error("DualKukaLWRFRIDriver::DualKukaLWRFRIDriver: unable to retrieve joint group " + joint_group);

        auto op_name = configuration["end-effector_point_name"];
        if (op_name)
        {
            point_ = robot.observationPoint(op_name.as<std::string>());
            if (not point_)
                throw std::runtime_error("DualKukaLWRFRIDriver::DualKukaLWRFRIDriver: unable to retrieve end-effector point name");
        }

        impl_ = std::make_unique<URDriver::pImpl>(joint_group_->controlTimeStep(),
                                                  ip, port);
    }
    else
    {
        throw std::runtime_error("URDriver::URDriver: The configuration file doesn't include a 'driver' field.");
    }
}

URDriver::~URDriver() = default;

bool URDriver::init(double timeout)
{
    impl_->driver.setCommandMode(ur::Driver::CommandMode::JointVelocityControl);

    read();

    return true;
}

bool URDriver::start()
{
    impl_->driver.setCommandMode(ur::Driver::CommandMode::Monitor);
    impl_->driver.start();

    sync();

    return true;
}

bool URDriver::stop()
{
    sync();
    impl_->driver.stop();

    return true;
}

bool URDriver::read()
{
    sync();

    std::lock_guard<std::mutex> lock(joint_group_->state_mtx_);

    impl_->driver.get_Data();

    jointGroupState().position() = impl_->robot.state.joint_positions.value();
    jointGroupState().force() = impl_->robot.state.joint_currents.value();

    if (point_)
    {
        std::lock_guard<std::mutex> lock(point_->state_mtx_);
        pointState().wrench() = impl_->robot.state.tcp_generalized_force.value();
    }
    //Approximation
    jointGroupState().velocity() = joint_group_->command().velocity();

    jointGroupLastStateUpdate() = std::chrono::high_resolution_clock::now();
    return true;
}

bool URDriver::send()
{

    std::lock_guard<std::mutex> lock(joint_group_->command_mtx_);

    impl_->robot.command.joint_velocities.value() = joint_group_->command().velocity();
    impl_->robot.command.joint_positions.value() = joint_group_->command().position();

    impl_->driver.send_Data();

    return true;
}

bool URDriver::sync()
{
    impl_->driver.sync();
    return true;
}
