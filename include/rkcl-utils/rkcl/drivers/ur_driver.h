/**
 * @file ur_driver.h
 * @date January 9, 2019
 * @author Benjamin Navarro
 * @brief Defines a simple UR driver
 */

#pragma once

#include <rkcl/drivers/joints_driver.h>
#include <rkcl/drivers/force_sensor_driver.h>
#include <memory>
#include <string>
#include <vector>

namespace YAML
{
class Node;
}

/**
 * @brief Namespace for everything related to the cooperative task space
 * representation and control
 */
namespace rkcl
{

/**
 * @brief Wrapper for the FRI library
 */
class URDriver : public JointsDriver, public ForceSensorDriver
{
public:
    URDriver(const std::string& ip, int port, JointGroupPtr joint_group,
             ObservationPointPtr op_eef);

    URDriver(const std::string& ip, int port, JointGroupPtr joint_group);

    URDriver(Robot& robot, const YAML::Node& configuration);

    virtual ~URDriver();

    /**
   * Initialize the communication with UR controllers
   * @param timeout The maximum time to wait to establish the connection.
   * @return true on success, false otherwise
   */
    virtual bool init(double timeout = 30.) override;

    /**
   * @brief Start the simulation.
   */
    virtual bool start() override;

    /**
   * @brief Stop the simulation.
   */
    virtual bool stop() override;

    /**
   * Update the robot's state with the data from the controllers
   * @return true on success, false otherwise
   */
    virtual bool read() override;

    /**
   * Send the robot's commands to the controllers
   * @return true on success, false otherwise
   */
    virtual bool send() override;

    /**
   * Wait for the synchronization signals sent by the controllers
   */
    virtual bool sync() override;

private:
    static bool registered_in_factory;
    struct pImpl;
    std::unique_ptr<pImpl> impl_;
};

using URDriverPtr = std::shared_ptr<URDriver>;
using URDriverConstPtr = std::shared_ptr<const URDriver>;
} // namespace rkcl
